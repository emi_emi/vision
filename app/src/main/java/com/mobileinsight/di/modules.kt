package com.mobileinsight.di;

import android.content.Context
import androidx.room.Room
import com.mobileinsight.api.ServiceGenerator
import com.mobileinsight.repository.UserContextRepository
import com.mobileinsight.repository.VisionDatabase
import com.mobileinsight.repository.SharedPreferencesProvider
import com.mobileinsight.repository.StoreRepository
import com.mobileinsight.services.*
import com.mobileinsight.services.impl.AuthenticatingServiceImpl
import com.mobileinsight.services.impl.StoreServiceImpl
import com.mobileinsight.services.impl.UserContextServiceImpl
import org.jetbrains.anko.defaultSharedPreferences
import org.koin.dsl.module

val appModule = module {
    // Room Database
    single {
        Room.databaseBuilder(get(), VisionDatabase::class.java, "vision-db")
                .build()
    }


    single { get<Context>().defaultSharedPreferences }

    single { SharedPreferencesProvider(sharedPreferences = get()) }
}

val daoModule = module {
    single { get<VisionDatabase>().userDao() }
    single { get<VisionDatabase>().storeDao() }
}

val networkModule = module {
    single { ServiceGenerator.createService(LoginProvider::class.java) }
    single { ServiceGenerator.createService(StoreProvider::class.java) }
}

val cacheModule = module {
    //todo add cache data here
}

val repositoryModule = module {
    single { UserContextRepository(userDao = get()) }
    single { StoreRepository(storeDao = get()) }
}

val serviceModule = module{
    single { AuthenticatingServiceImpl(loginProvider = get()) as AuthenticatingService }
    single { UserContextServiceImpl(userContextRepository = get()) as UserContextService }
    single { StoreServiceImpl(storeProvider = get(), storeRepository = get()) as StoreService }
}
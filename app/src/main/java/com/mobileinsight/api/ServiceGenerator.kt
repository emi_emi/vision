package com.mobileinsight.api

import android.text.TextUtils
import com.mobileinsight.services.BaseService.Companion.SERVER_ENDPOINT
import okhttp3.Credentials
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit.Builder
import retrofit2.converter.gson.GsonConverterFactory

object ServiceGenerator {
    private val API_BASE_URL = "https://$SERVER_ENDPOINT"

    private val httpClient = OkHttpClient.Builder()

    private val builder: Builder = Builder()
            .baseUrl(API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())

    private var retrofit = builder.build()

    fun <S> createService(serviceClass: Class<S>): S {
        return createService(serviceClass, null, null)
    }

    fun <S> createService(serviceClass: Class<S>, username: String?, password: String?): S {
        if (username != null && password != null) {
            val authToken = Credentials.basic(username, password)
            return createService(serviceClass, authToken)
        }

        return createService(serviceClass, null)
    }

    fun <S> createService(serviceClass: Class<S>, authToken: String?): S {
        if (authToken != null) {
            val interceptor = AuthenticationInterceptor(authToken)
            val loggingInterceptor =HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            if (!httpClient.interceptors().contains(interceptor)) {
                httpClient.addInterceptor(interceptor)
                httpClient.addInterceptor(loggingInterceptor)
                builder.client(httpClient.build())
                retrofit = builder.build()
            }
        }

        return retrofit.create(serviceClass)
    }
}


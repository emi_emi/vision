package com.mobileinsight.repository

import android.content.SharedPreferences

class SharedPreferencesProvider (private val sharedPreferences: SharedPreferences) {

    fun saveData(key : String, value : Any) {
        val editor = sharedPreferences.edit()
        when(value) {
            is Boolean -> editor.putBoolean(key, value)
            is String -> editor.putString(key, value)
            is Long -> editor.putLong(key, value)
            is Int -> editor.putInt(key, value)
            is Float -> editor.putFloat(key, value)
        }
         editor.apply()
    }

    fun getData(key : String) : Any? {
        return sharedPreferences.all[key]
    }

}
package com.mobileinsight.repository

import androidx.lifecycle.LiveData
import com.mobileinsight.repository.dao.UserDao
import com.mobileinsight.repository.entities.UserContext

class UserContextRepository(private val userDao: UserDao) {

    /**This method is called on login to save user data in db*/
    suspend fun saveUser(userContext: UserContext) {
        userDao.insert(userContext)
    }

    /**This method is called on logout to clear user's data*/
    suspend fun deleteUser() {
        userDao.clearUserData()
    }

    fun getUser() : LiveData<UserContext> {
       return userDao.getUser()
    }
}
package com.mobileinsight.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.mobileinsight.repository.entities.UserContext

@Dao
interface UserDao : BaseDao<UserContext> {

    @Query("DELETE FROM user_table")
    suspend fun clearUserData()

    @Query("SELECT * FROM user_table")
    fun getUser() : LiveData<UserContext>

}
package com.mobileinsight.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.mobileinsight.repository.entities.Store

@Dao
interface StoreDao : BaseDao<Store> {

    @Query("SELECT * FROM store_table")
    fun getAllStores() : LiveData<List<Store>>

    @Query("SELECT * FROM store_table WHERE id=:storeId")
    fun getStoreById(storeId : Int) : LiveData<Store>
}
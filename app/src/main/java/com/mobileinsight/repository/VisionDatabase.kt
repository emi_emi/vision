package com.mobileinsight.repository

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.mobileinsight.repository.dao.StoreDao
import com.mobileinsight.repository.dao.UserDao
import com.mobileinsight.repository.entities.Store
import com.mobileinsight.repository.entities.UserContext
import com.mobileinsight.utils.RoomConverter

@Database(entities = [UserContext::class, Store::class], version = 1, exportSchema = false)
@TypeConverters(RoomConverter::class)
abstract class VisionDatabase : RoomDatabase() {

    public abstract fun userDao() : UserDao

    public abstract fun storeDao() : StoreDao


}
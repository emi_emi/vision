package com.mobileinsight.repository.entities


import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.mobileinsight.services.model.StoreOpenHour

@Entity(tableName = "store_table")
data class Store(

        @PrimaryKey @SerializedName("id")
        val id: Int,
        @SerializedName("accountId")
        val accountId: Int,
        @SerializedName("accountName")
        val accountName: String,
        @SerializedName("active")
        val active: Boolean,
        @SerializedName("address")
        val address: String,
        @SerializedName("applyCompliance")
        val applyCompliance: Boolean,
        @SerializedName("bossUsername")
        val bossUsername: String,
        @SerializedName("certifiedStoreStatus")
        val certifiedStoreStatus: Boolean,
        @SerializedName("city")
        val city: String,
        @SerializedName("cluster")
        val cluster: String,
        @SerializedName("countryId")
        val countryId: Int,
        @SerializedName("countryName")
        val countryName: String,
        @SerializedName("division")
        val division: String,
        @SerializedName("effectiveDate")
        val effectiveDate: String,
        @SerializedName("effectiveDateNew")
        val effectiveDateNew: String,
        @SerializedName("futureVisitFrequency")
        val futureVisitFrequency: Int,
        @SerializedName("futureVisitPeriod")
        val futureVisitPeriod: String,
        @SerializedName("geofence")
        val geofence: Int,
        @SerializedName("latitude")
        val latitude: Int,
        @SerializedName("longitude")
        val longitude: Int,
        @SerializedName("nickname")
        val nickname: String,
        @SerializedName("phoneNumber")
        val phoneNumber: String,
        @SerializedName("region")
        val region: String,
        @SerializedName("stateId")
        val stateId: Int,
        @SerializedName("stateName")
        val stateName: String,
        @SerializedName("storeAssignedOn")
        val storeAssignedOn: String,
        @SerializedName("storeCreatedOn")
        val storeCreatedOn: String,
        @SerializedName("storeDesignation")
        val storeDesignation: String,
        @SerializedName("storeDesignationId")
        val storeDesignationId: Int,
        @SerializedName("storeName")
        val storeName: String,
        @SerializedName("storeNumber")
        val storeNumber: String,
        @SerializedName("storeOpenHours")
        val storeOpenHours: List<StoreOpenHour>,
        @SerializedName("storeOwner")
        val storeOwner: String,
        @SerializedName("storeOwnerUserId")
        val storeOwnerUserId: Int,
        @SerializedName("storeType")
        val storeType: String,
        @SerializedName("storeTypeId")
        val storeTypeId: Int,
        @SerializedName("storeUpdatedOn")
        val storeUpdatedOn: String,
        @SerializedName("storeUsers")
        val storeUsers: List<Int>,
        @SerializedName("timeZone")
        val timeZone: String,
        @SerializedName("visitFrequency")
        val visitFrequency: Int,
        @SerializedName("visitPeriod")
        val visitPeriod: String,
        @SerializedName("zipCode")
        val zipCode: String
)
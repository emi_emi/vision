package com.mobileinsight.repository.entities


import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.mobileinsight.ui.authentication.model.Organization

@Entity(tableName = "user_table")
data class UserContext(
        @SerializedName("email")
    val email: String,
        @SerializedName("firstName")
    val firstName: String,
        @SerializedName("lastName")
    val lastName: String,
        @SerializedName("organizations")
    val organizations: List<Organization>,
        @SerializedName("passwordExpiresOn")
    val passwordExpiresOn: String,
        @SerializedName("passwordReset")
    val passwordReset: Boolean,
        @PrimaryKey
    @SerializedName("userId")
    val userId: Int
)
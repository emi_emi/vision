package com.mobileinsight.services

import com.mobileinsight.ui.authentication.model.UserLoginResponse
import com.mobileinsight.utils.Event

interface AuthenticatingService : BaseService{

    suspend fun login(userName : String, password : String) : UserLoginResponse
}
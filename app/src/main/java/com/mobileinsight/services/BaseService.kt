package com.mobileinsight.services

import org.koin.core.KoinComponent


interface BaseService : KoinComponent {
    companion object {
        var SERVER_ENDPOINT = "qa-api.mobileinsight.com"
        val URL_ENDPOINT_KEY = "ulr_endpoint_key"
    }
}
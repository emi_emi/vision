package com.mobileinsight.services.impl

import com.mobileinsight.repository.entities.UserContext
import com.mobileinsight.services.AuthenticatingService
import com.mobileinsight.services.LoginProvider
import com.mobileinsight.ui.authentication.model.LoginBody
import com.mobileinsight.ui.authentication.model.UserLoginResponse
import kotlinx.coroutines.suspendCancellableCoroutine
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.resume

class AuthenticatingServiceImpl(private val loginProvider: LoginProvider) : AuthenticatingService {

    override suspend fun login(userName: String, password: String)
            : UserLoginResponse = suspendCancellableCoroutine { continuation ->
        loginProvider.login(LoginBody(password, userName))
                .enqueue(object : Callback<UserContext> {
                    override fun onFailure(call: Call<UserContext>, t: Throwable) {
                        continuation.resume(UserLoginResponse(false, null))
                    }

                    override fun onResponse(call: Call<UserContext>, response: Response<UserContext>) {
                        if (response.isSuccessful) {
                            continuation.resume(UserLoginResponse(true, response.body()))
                        } else {
                            continuation.resume(UserLoginResponse(false, null))
                        }
                    }
                })
    }
}
package com.mobileinsight.services.impl

import androidx.lifecycle.LiveData
import com.mobileinsight.repository.UserContextRepository
import com.mobileinsight.services.UserContextService
import com.mobileinsight.repository.entities.UserContext

class UserContextServiceImpl(val userContextRepository : UserContextRepository) : UserContextService {
    override fun getUser(): LiveData<UserContext> {
        return userContextRepository.getUser()
    }

    override suspend fun saveUser(userContext: UserContext) {
        userContextRepository.saveUser(userContext)
    }

    override suspend fun deleteUser() {
        userContextRepository.deleteUser()
    }
}
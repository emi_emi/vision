package com.mobileinsight.services.model


import com.google.gson.annotations.SerializedName
import com.mobileinsight.repository.entities.Store

data class StoreResponse(
        @SerializedName("list")
    val list: List<Store>,
        @SerializedName("pageNumber")
    val pageNumber: Int,
        @SerializedName("pageSize")
    val pageSize: Int,
        @SerializedName("startIndex")
    val startIndex: Int,
        @SerializedName("TotalRowCount")
    val totalRowCount: Int
)
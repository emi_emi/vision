package com.mobileinsight.services.model


import androidx.room.Entity
import com.google.gson.annotations.SerializedName
@Entity
data class OpenHour(
    @SerializedName("dayOfWeek")
    val dayOfWeek: Int,
    @SerializedName("endTime")
    val endTime: String,
    @SerializedName("startTime")
    val startTime: String
)
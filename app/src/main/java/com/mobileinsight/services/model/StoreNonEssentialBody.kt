package com.mobileinsight.services.model


import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import com.mobileinsight.services.model.OpenHour

@Entity
data class StoreNonEssentialBody(
        @SerializedName("latitude")
    val latitude: Int,
        @SerializedName("longitude")
    val longitude: Int,
        @SerializedName("nickname")
    val nickname: String,
        @SerializedName("openHours")
    val openHours: List<OpenHour>,
        @SerializedName("phoneNumber")
    val phoneNumber: String
)
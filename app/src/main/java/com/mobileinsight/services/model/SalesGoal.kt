package com.mobileinsight.services.model


import androidx.room.Entity
import com.google.gson.annotations.SerializedName
@Entity
data class SalesGoal(
    @SerializedName("currentMonthActual")
    val currentMonthActual: String,
    @SerializedName("currentMonthGoal")
    val currentMonthGoal: String,
    @SerializedName("currentMonthPercent")
    val currentMonthPercent: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("doesCurrentMonthActualExist")
    val doesCurrentMonthActualExist: Boolean,
    @SerializedName("doesCurrentMonthGoalExist")
    val doesCurrentMonthGoalExist: Boolean,
    @SerializedName("doesPreviousMonthActualExist")
    val doesPreviousMonthActualExist: Boolean,
    @SerializedName("doesPreviousMonthGoalExist")
    val doesPreviousMonthGoalExist: Boolean,
    @SerializedName("id")
    val id: Int,
    @SerializedName("lastUpdatedDate")
    val lastUpdatedDate: String,
    @SerializedName("mappedColumn")
    val mappedColumn: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("previousMonthActual")
    val previousMonthActual: String,
    @SerializedName("previousMonthGoal")
    val previousMonthGoal: String,
    @SerializedName("previousMonthPercent")
    val previousMonthPercent: String,
    @SerializedName("quotaType")
    val quotaType: String,
    @SerializedName("showPercentage")
    val showPercentage: Boolean,
    @SerializedName("showPreviousPeriod")
    val showPreviousPeriod: Boolean,
    @SerializedName("sortOrder")
    val sortOrder: Int,
    @SerializedName("valueType")
    val valueType: String
)
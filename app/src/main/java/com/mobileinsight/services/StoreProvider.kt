package com.mobileinsight.services

import com.mobileinsight.services.model.SalesGoal
import com.mobileinsight.services.model.StoreNonEssentialBody
import com.mobileinsight.repository.entities.Store
import com.mobileinsight.services.model.StoreResponse
import retrofit2.Call
import retrofit2.http.*

interface StoreProvider {
    @GET("api/organizations/{orgId}/stores")
    fun getStores(@Path("orgId") orgId: Int,
                  @Query("startIndex") startIndex: Int? = null,
                  @Query("pageSize") pageSize: Int? = null,
                  @Query("assignedTo") assignedTo: String? = null,
                  @Query("daysWithNoVisit") days: Int? = null)
            : Call<StoreResponse>

    @GET("api/organizations/{orgId}/stores/groupStores/{groupId}")
    fun getGroupStores(@Path("orgId") orgId: Int,
                       @Path("groupId") groupId: Int,
                       @Query("startIndex") startIndex: Int? = null,
                       @Query("pageSize") pageSize: Int? = null,
                       @Query("daysWithNoVisit") daysWithNoVisit: IntArray? = null)
            : Call<StoreResponse>

    @GET("api/organizations/{orgId}/stores/{storeId}")
    fun getStoreById(@Path("orgId") orgId: Int, @Path("storeId") storeId: Int): Call<Store>

    @POST("api/organizations/{orgId}/stores/{storeId}/certifyStore/v2")
    fun certifyStore(@Path("orgId") orgId: Int, @Path("storeId") storeId: Int,
                     @Body storeNonEssential: StoreNonEssentialBody): Call<Void>

    @POST("api/organizations/{orgId}/stores/{storeId}/nonEssential/v2")
    fun updateStoreNonEssentialDetails(@Path("orgId") orgId: Int,
                                       @Path("storeId") storeId: Int,
                                       @Body storeNonEssential: StoreNonEssentialBody?): Call<Void>

    @GET("api/organizations/{orgId}/stores/{storeId}/salesGoals")
    fun getSalesGoalsData(@Path("orgId") orgId: Int, @Path("storeId") storeId: Int)
            : Call<List<SalesGoal>>

//    @GET("api/organizations/{orgId}/storeVisitEventTypes")
//    fun getStoreVisitEventTypes(@Path("orgId") orgId: Int): Call<List<EventType>>

    @GET("api/organizations/{orgId}/stores/activityStores")
    fun getActivityStores(@Path("orgId") orgId: Int,
                          @Query("startIndex") startIndex: Int? = 0,
                          @Query("pageSize") pageSize: Int? = 0,
                          @Query("assignedTo") assignedTo: String? = null): Call<StoreResponse>

    @GET("api/organizations/{orgId}/stores/activityStores")
    fun getActivityStores(@Path("orgId") orgId: Int,
                          @Query("startIndex") startIndex: Int?,
                          @Query("pageSize") pageSize: Int?,
                          @Query("assignedTo") assignedTo: String?,
                          @Query("userIds") userIds: String?): Call<StoreResponse>
}
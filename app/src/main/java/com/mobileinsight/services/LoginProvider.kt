package com.mobileinsight.services

import com.mobileinsight.ui.authentication.model.LoginBody
import com.mobileinsight.repository.entities.UserContext
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginProvider {
    @POST("api/login")
    fun login(@Body loginBody: LoginBody): Call<UserContext>
}
package com.mobileinsight.services

import androidx.lifecycle.LiveData
import com.mobileinsight.repository.entities.UserContext

interface UserContextService : BaseService {

    fun getUser() : LiveData<UserContext>

    suspend fun saveUser(userContext: UserContext)

    suspend fun deleteUser()
}
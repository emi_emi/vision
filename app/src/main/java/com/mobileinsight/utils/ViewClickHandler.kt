package com.mobileinsight.utils

import android.view.View

class ViewClickHandler(private val clickHandler: (view: View) -> Unit) {
    fun onViewClick(view: View) {
        clickHandler.invoke(view)
    }
}

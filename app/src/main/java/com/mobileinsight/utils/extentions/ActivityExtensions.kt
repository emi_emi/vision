package com.mobileinsight.utils.extentions

import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager

fun Activity.hideKeyboard() {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    val focusedView = currentFocus ?: View(this)

    inputMethodManager.hideSoftInputFromWindow(focusedView.windowToken, 0)
}

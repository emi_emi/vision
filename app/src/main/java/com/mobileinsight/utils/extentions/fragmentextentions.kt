package com.mobileinsight.utils.extentions

import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController

fun Fragment.navigateTo(direction: NavDirections) {
    findNavController().navigate(direction)
}
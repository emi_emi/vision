package com.mobileinsight.utils

import com.anychart.charts.CircularGauge
import com.anychart.enums.Anchor
import com.anychart.graphics.vector.SolidFill
import com.anychart.graphics.vector.text.HAlign
import com.anychart.graphics.vector.text.VAlign

fun CircularGauge.getDefaultCircularGauge(): CircularGauge {
    return fill("#fff")
            .stroke(null)
            .padding(0, 0, 0, 0)
            .margin(100, 100, 100, 100)
}

fun CircularGauge.addNewBar(labelName: String, barIndex: Int, offset: Int, barColor: String): CircularGauge {
    label(barIndex)
            .text(labelName)
            .useHtml(true)
            .hAlign(HAlign.CENTER)
            .vAlign(VAlign.MIDDLE)
    label(barIndex)
            .anchor(Anchor.RIGHT_CENTER)
            .padding(0, 10, 0, 0)
            .height((17 / 2).toString() + "%")
            .offsetY("$offset%")
            .offsetX(0);

    bar(barIndex)
            .dataIndex(barIndex)
            .radius(offset)
            .width(17)
            .fill(SolidFill(barColor, 1))
            .stroke(null)
            .zIndex(5)

    bar(100)
            .dataIndex(5)
            .radius(100)
            .width(17)
            .fill(SolidFill("#F5F4F4", 1))
            .stroke("1 #e5e4e4")
            .zIndex(4)

    return this
}
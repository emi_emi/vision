package com.mobileinsight.utils;

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.mobileinsight.R
import com.squareup.picasso.Picasso

/** Extension  for inflating the layout resource. Can be used in Adapters*/
fun ViewGroup.inflate(layoutRes: Int): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, false)
}

/** Extension for loading image url with Picasso */
fun ImageView.loadUrl(url: String?) {
    Picasso.get().load(url)
            .placeholder(R.drawable.ic_launcher_foreground)
            .into(this);
}
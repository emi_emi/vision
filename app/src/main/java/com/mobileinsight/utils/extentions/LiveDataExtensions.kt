package com.mobileinsight.utils

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

fun <T : Any> MutableLiveData<Event<T>>.updateValue(newValue: T) {
    value = Event(newValue)
}

fun MutableLiveData<Event<Unit>>.trigger() {
    value = Event(Unit)
}

fun <T : Any> LiveData<Event<T>>.observeValue(owner: LifecycleOwner, observer: (T) -> Unit) {
    observe(owner, Observer {
        val value = it?.getContentIfNotHandled()
        if (value != null) {
            observer(value)
        }
    })
}

fun <T : Any> LiveData<T>.observeNonNull(owner: LifecycleOwner, observer: (T) -> Unit) {
    observe(owner, Observer {
        if (it != null) {
            observer(it)
        }
    })
}

package com.mobileinsight.utils

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mobileinsight.repository.entities.Store
import com.mobileinsight.services.model.StoreOpenHour
import com.mobileinsight.ui.authentication.model.Organization

object RoomConverter {

    @TypeConverter
    @JvmStatic
    fun organizationFromList(list : List<Organization>) : String {
        return Gson().toJson(list, object : TypeToken<List<Organization>>(){}.type)
    }

    @TypeConverter
    @JvmStatic
    fun organizationToList(jsonString : String) : List<Organization> {
        return Gson().fromJson(jsonString, object : TypeToken<List<Organization>>(){}.type)
    }

    @TypeConverter
    @JvmStatic
    fun storeFromList(list : List<Store>) : String {
        return Gson().toJson(list, object : TypeToken<List<Store>>(){}.type)
    }

    @TypeConverter
    @JvmStatic
    fun storeToList(jsonString : String) : List<Store> {
        return Gson().fromJson(jsonString, object : TypeToken<List<Store>>(){}.type)
    }

    @TypeConverter
    @JvmStatic
    fun storeOpenHourFromList(list : List<StoreOpenHour>) : String {
        return Gson().toJson(list, object : TypeToken<List<StoreOpenHour>>(){}.type)
    }

    @TypeConverter
    @JvmStatic
    fun storeOpenHourToList(jsonString : String) : List<StoreOpenHour> {
        return Gson().fromJson(jsonString, object : TypeToken<List<StoreOpenHour>>(){}.type)
    }

    @TypeConverter
    @JvmStatic
    fun intFromList(list : List<Int>) : String {
        return Gson().toJson(list, object : TypeToken<List<Int>>(){}.type)
    }

    @TypeConverter
    @JvmStatic
    fun intToList(jsonString : String) : List<Int> {
        return Gson().fromJson(jsonString, object : TypeToken<List<Int>>(){}.type)
    }
}
package com.mobileinsight.utils

/**
 * A generic event that may or may not have been handled
 */
class Event<out T>(private val content: T) {
    private var handled = false

    fun getContentIfNotHandled(): T? {
        if (handled) {
            return null
        }

        handled = true
        return content
    }

    fun peekContent(): T = content
}
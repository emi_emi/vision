package com.mobileinsight.ui.activities

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mobileinsight.databinding.ActivitiesFragmentBinding
import com.mobileinsight.ui.contracts.BaseFragment

class ActivitiesFragment : BaseFragment() {

    lateinit var binding: ActivitiesFragmentBinding

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?
    ): View? {
        binding = ActivitiesFragmentBinding.inflate(layoutInflater)
        return binding.root
    }
}
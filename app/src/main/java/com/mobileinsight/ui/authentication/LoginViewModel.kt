package com.mobileinsight.ui.authentication

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.mobileinsight.BaseViewModel
import com.mobileinsight.services.AuthenticatingService
import com.mobileinsight.services.UserContextService
import com.mobileinsight.utils.Event
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.core.inject

class LoginViewModel : BaseViewModel() {

    private val authenticationService by inject<AuthenticatingService>()
    private val userContextService by inject<UserContextService>()

    private val _isUserLoggedIn = MutableLiveData<Event<Boolean>>()
    val isUserLoggedIn: LiveData<Event<Boolean>> = _isUserLoggedIn

    fun login(username: String, password: String)  =
        viewModelScope.launch {
            val result = authenticationService.login(username, password)
            if (result.userContext != null) {
                withContext(Dispatchers.Default){
                    userContextService.saveUser(result.userContext)
                }
            }
            _isUserLoggedIn.value = Event(result.loggedIn)
        }



}
package com.mobileinsight.ui.authentication.model

import com.mobileinsight.repository.entities.UserContext

data class UserLoginResponse(val loggedIn : Boolean , val userContext : UserContext?)
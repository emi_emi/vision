package com.mobileinsight.ui.authentication

import androidx.lifecycle.LiveData
import com.mobileinsight.BaseViewModel
import com.mobileinsight.repository.entities.UserContext
import com.mobileinsight.services.UserContextService
import org.koin.core.inject

class StartViewModel : BaseViewModel() {

    private val userContextService by inject<UserContextService>()

    fun loadUser() : LiveData<UserContext>{
        return userContextService.getUser()
    }
}
package com.mobileinsight.ui.authentication

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.mobileinsight.databinding.DialogBottomSettingsBinding
import com.mobileinsight.di.networkModule
import com.mobileinsight.services.BaseService
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.koin.core.context.loadKoinModules

class ServiceUrlBottomDialog : BottomSheetDialogFragment() {

    lateinit var binding: DialogBottomSettingsBinding

    private val viewModel by lazy { ViewModelProvider(this).get(ServiceUrlViewModel::class.java) }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DialogBottomSettingsBinding.inflate(layoutInflater)
        binding.textInputUrl.setText(BaseService.SERVER_ENDPOINT)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        viewModel.saveEndpoint(binding.textInputUrl.text.toString())
    }

    companion object {
        fun getInstance() : ServiceUrlBottomDialog {
            val instance : ServiceUrlBottomDialog by lazy { ServiceUrlBottomDialog() }
            return instance
        }
    }
}
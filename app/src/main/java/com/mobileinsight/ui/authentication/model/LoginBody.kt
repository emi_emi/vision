package com.mobileinsight.ui.authentication.model


import com.google.gson.annotations.SerializedName

data class LoginBody(
    @SerializedName("password")
    val password: String,
    @SerializedName("username")
    val username: String
)
package com.mobileinsight.ui.authentication

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.mobileinsight.R
import com.mobileinsight.databinding.ActivityLoginBinding
import com.mobileinsight.ui.MainActivity
import com.mobileinsight.utils.extentions.hideKeyboard
import com.mobileinsight.utils.observeValue
import org.jetbrains.anko.toast

class LoginActivity : AppCompatActivity() {

    lateinit var binding: ActivityLoginBinding
    private val viewModel by lazy { ViewModelProvider(this).get(LoginViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding.buttonConnect.setOnClickListener {
            hideKeyboard()
            binding.authenticatingView.visibility = View.VISIBLE
            viewModel.login(binding.textInputUserName.text.toString(),
                    binding.editTextPassword.text.toString())
        }

        binding.textInputUserName.setText("mr000005")
        binding.editTextPassword.setText("Beatles1!")

        binding.textViewForgotPassword.setOnClickListener {
            toast("Forgot password clicked")
        }

        binding.imageViewSettingsIcon.setOnClickListener {
            ServiceUrlBottomDialog.getInstance()
                    .show(supportFragmentManager, "Change URL dialog")
        }
        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewModel.isUserLoggedIn.observeValue(this) {
            if (it) {
                //todo first fetch all stores and cache them and afterwards do the rest of the calls
                navigateToMainScreen()
            } else {
                binding.authenticatingView.visibility = View.GONE
                toast("Authentication failed")
            }
        }
    }

    private fun navigateToMainScreen() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}
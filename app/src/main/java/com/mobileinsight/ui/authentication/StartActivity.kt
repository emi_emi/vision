package com.mobileinsight.ui.authentication

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.mobileinsight.ui.MainActivity

class StartActivity : AppCompatActivity() {

    private val viewModel by lazy { ViewModelProvider(this).get(StartViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.loadUser().observe(this, Observer {
            openApplication(it != null)
        })
    }

    private fun openApplication(isLoggedIn: Boolean) {
        if(isLoggedIn) navigateToMainScreen() else navigateToLoginScreen()
    }

    private fun navigateToMainScreen() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    private fun navigateToLoginScreen() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }
}
package com.mobileinsight.ui.authentication.model


import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Organization(
    @SerializedName("compliance")
    val compliance: Boolean,
    @SerializedName("complianceInWidget")
    val complianceInWidget: Boolean,
    @SerializedName("contributionInWidget")
    val contributionInWidget: Boolean,
    @PrimaryKey
    @SerializedName("id")
    val id: Int,
    @SerializedName("orgName")
    val orgName: String,
    @SerializedName("weeklyComplianceStartDay")
    val weeklyComplianceStartDay: String
)
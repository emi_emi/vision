package com.mobileinsight.ui.authentication

import com.mobileinsight.BaseViewModel
import com.mobileinsight.services.BaseService.Companion.URL_ENDPOINT_KEY

class ServiceUrlViewModel : BaseViewModel() {

    fun saveEndpoint(endpointString: String) {
        sharedPreferencesProvider.saveData(URL_ENDPOINT_KEY, endpointString)
    }
}
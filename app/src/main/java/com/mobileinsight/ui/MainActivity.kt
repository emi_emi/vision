package com.mobileinsight.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mobileinsight.BottomNavigationViewManager
import com.mobileinsight.R
import kotlinx.android.synthetic.main.activity_main.mainBottomNavigationView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupBottomNavigationView()
    }

    private fun setupBottomNavigationView() {
        val menuItemIdToNavGraphResourceIdMap = listOf(
                R.id.action_today to R.navigation.navigation_graph_today,
                R.id.action_stores to R.navigation.navigation_graph_stores,
                R.id.action_visits to R.navigation.navigation_graph_visits,
                R.id.action_calendar to R.navigation.navigation_graph_calendar,
                R.id.action_activities to R.navigation.navigation_graph_activities
        )

        BottomNavigationViewManager(mainBottomNavigationView, supportFragmentManager).setup(
                R.id.navHostFragmentContainer,
                menuItemIdToNavGraphResourceIdMap.toMap()
        )
    }
}
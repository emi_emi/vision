package com.mobileinsight.ui.visits

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mobileinsight.databinding.FragmentVisitsBinding
import com.mobileinsight.ui.contracts.BaseFragment

class VisitsFragment : BaseFragment() {

    lateinit var binding: FragmentVisitsBinding

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?
    ): View? {
        binding = FragmentVisitsBinding.inflate(layoutInflater)
        return binding.root
    }
}
package com.mobileinsight.ui.today

import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.mobileinsight.BaseViewModel
import com.mobileinsight.repository.entities.UserContext
import com.mobileinsight.services.UserContextService
import kotlinx.coroutines.launch
import org.koin.core.inject

class TodayViewModel : BaseViewModel() {
    private val userContextService by inject<UserContextService>()

    fun onLogoutClick() =
        viewModelScope.launch {
            //todo this will be mode on userprofile screen and will delete entire db
            userContextService.deleteUser()
        }

    fun loadUserContext(): LiveData<UserContext> {
         return userContextService.getUser()
    }

}
package com.mobileinsight.ui.today

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.anychart.AnyChart
import com.anychart.chart.common.dataentry.SingleValueDataSet
import com.anychart.graphics.vector.SolidFill
import com.mobileinsight.R
import com.mobileinsight.databinding.FragmentTodayBinding
import com.mobileinsight.ui.authentication.StartActivity
import com.mobileinsight.ui.contracts.BaseFragment
import com.mobileinsight.utils.ViewClickHandler
import com.mobileinsight.utils.addNewBar
import com.mobileinsight.utils.getDefaultCircularGauge
import org.jetbrains.anko.support.v4.toast


class TodayFragment : BaseFragment() {

    lateinit var binding: FragmentTodayBinding
    private val viewModel by lazy { ViewModelProvider(this).get(TodayViewModel::class.java) }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTodayBinding.inflate(layoutInflater)
        binding.initViews()
        binding.initClickListeners()
        initObservers()
        return binding.root
    }

    private fun initObservers() {
        viewModel.loadUserContext().observe(viewLifecycleOwner, Observer {
            if (it != null) binding.userDataCardView.textViewSubHead.text = it.firstName
                    .plus(" ").plus(it.lastName)
        })
    }

    private fun FragmentTodayBinding.initViews() {
        eventTitle = getString(R.string.events)
        eventsMessage = getString(R.string.searching_events)
        tasksTitle = getString(R.string.tasks)
        tasksMessage = getString(R.string.searching_tasks)
//        initMetrics()
    }

    private fun initMetrics() {
        val anyChartView = binding.metricsCardView.chartView
        val circularGauge = AnyChart.circular().getDefaultCircularGauge()
        circularGauge.data(SingleValueDataSet(arrayOf("23", "34", "67", "93", "56", "100")))

        circularGauge.startAngle(0)
        circularGauge.sweepAngle(270)

        val xAxis = circularGauge.axis(0).radius(100)
                .width(1)
                .fill(SolidFill("#fff", 0))

        xAxis.scale().minimum(0).maximum(100)
        xAxis.ticks("{interval: 1")
        xAxis.labels().enabled(false)
        xAxis.ticks().enabled(false)
        xAxis.minorTicks().enabled(false)

        circularGauge.addNewBar("GPS, 23%", 0, 100, "#64b5f6")
        circularGauge.addNewBar("Complience, 34%", 1, 80, "#1976d2")
        circularGauge.addNewBar("Contribution, 67%", 2, 60, "#ef6c00")
        circularGauge.addNewBar("Goal Visits to Date, 93%", 3, 40, "#ffd54f")
        circularGauge.addNewBar("Actual Qualified Visits, 56%", 4, 20, "#455a64")

        circularGauge.margin(8, 8, 8, 8)

        anyChartView.setChart(circularGauge)
    }

    private fun FragmentTodayBinding.initClickListeners() {
        adHocAction.handler = onAddNewAdHocForm()
        scheduleEvent.handler = onScheduleEvent()
        userDataCardView.handler = onSettingsClick()
        eventsCardView.handler = onEventsClick()
        tasksCardView.handler = onTasksClick()
    }

    private fun onAddNewAdHocForm(): ViewClickHandler {
        return ViewClickHandler {
            toast("Add new ad hoc form")
        }
    }

    private fun onScheduleEvent(): ViewClickHandler {
        return ViewClickHandler {
            toast("Schedule new event")
        }
    }

    private fun onSettingsClick(): ViewClickHandler {
        return ViewClickHandler {
            toast("Settings click")
            viewModel.onLogoutClick()
            navigateToStartActivity()
        }
    }

    private fun navigateToStartActivity() {
        requireActivity().startActivity(Intent(context, StartActivity::class.java))
        requireActivity().finish()
    }

    private fun onEventsClick(): ViewClickHandler {
        return ViewClickHandler {
            toast("Navigate to all events")
        }
    }

    private fun onTasksClick(): ViewClickHandler {
        return ViewClickHandler {
            toast("Navigate to all tasks")
        }
    }
}
package com.mobileinsight.ui.calendar

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mobileinsight.databinding.FragmentCalendarBinding
import com.mobileinsight.ui.contracts.BaseFragment

class CalendarFragment : BaseFragment() {

    lateinit var binding: FragmentCalendarBinding

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCalendarBinding.inflate(layoutInflater)
        return binding.root
    }
}
package com.mobileinsight.ui.stores
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.mobileinsight.R
import com.mobileinsight.databinding.FragmentStoresBinding
import com.mobileinsight.ui.contracts.BaseFragment
import kotlinx.android.synthetic.main.fragment_stores.*

class StoresFragment : BaseFragment(), OnMapReadyCallback {

    lateinit var binding: FragmentStoresBinding

    lateinit var googleMap : GoogleMap

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?
    ): View? {
        binding = FragmentStoresBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onMapReady(map: GoogleMap?) {
        map?.let { googleMap = map }

    }
}
package com.mobileinsight;

import androidx.multidex.MultiDexApplication
import com.mobileinsight.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class VisionApp : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        // Start Koin
        startKoin {
            androidLogger()
            androidContext(this@VisionApp)
            modules(appModule, daoModule, networkModule, repositoryModule, serviceModule)
        }
    }
}
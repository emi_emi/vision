package com.mobileinsight

import androidx.lifecycle.ViewModel
import com.mobileinsight.di.networkModule
import com.mobileinsight.repository.SharedPreferencesProvider
import com.mobileinsight.services.BaseService
import org.koin.core.KoinComponent
import org.koin.core.context.loadKoinModules
import org.koin.core.inject

abstract class BaseViewModel  : ViewModel(), KoinComponent {
   val sharedPreferencesProvider by inject<SharedPreferencesProvider>()

   fun loadServerEndpoint(){
      val endpoint = sharedPreferencesProvider.getData(BaseService.URL_ENDPOINT_KEY) as String?
      BaseService.SERVER_ENDPOINT = endpoint?: BaseService.SERVER_ENDPOINT
   }
}
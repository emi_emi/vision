package com.mobileinsight

import android.view.MenuItem
import androidx.annotation.IdRes
import androidx.fragment.app.FragmentManager
import androidx.navigation.fragment.NavHostFragment
import com.google.android.material.bottomnavigation.BottomNavigationView


/**
 * Class that manages the the navigation for the given [BottomNavigationView]
 *
 * Heavily based on samples from Google:
 * https://github.com/android/architecture-components-samples/tree/master/NavigationAdvancedSample
 */
class BottomNavigationViewManager(
        private val bottomNavigationView: BottomNavigationView,
        private val fragmentManager: FragmentManager
) {
    private lateinit var firstNavHostFragmentTag: String

    private lateinit var selectedNavHostFragmentTag: String

    private lateinit var firstNavHostFragment: NavHostFragment

    private lateinit var navHostFragments: List<NavHostFragment>

    private val navigationItemSelectedListener by lazy { NavigationItemSelectedListener() }

    private var firstMenuItemId = -1

    private val menuItemIdToNavHostFragmentTagMap = mutableMapOf<Int, String>()

    /**
     * Ties each tab in the given [BottomNavigationView], to [NavHostFragment] instances configured with each of the
     * provided navigation graphs
     */
    fun setup(
            @IdRes navHostFragmentContainerId: Int,
            menuItemIdToNavGraphResourceIdMap: Map<Int, Int>
    ) {
        require(bottomNavigationView.menu.size() == menuItemIdToNavGraphResourceIdMap.size) {
            "The number of menu items in the BottomNavigationView instance doesn't match the number " +
                    "of items in the supplied map"
        }

        firstMenuItemId = menuItemIdToNavGraphResourceIdMap.keys.first()

        initMenuItemIdToNavHostFragmentTagMap(menuItemIdToNavGraphResourceIdMap)

        val navGraphResourcesIds = menuItemIdToNavGraphResourceIdMap.values
        navHostFragments = createNavHostFragments(fragmentManager, navHostFragmentContainerId, navGraphResourcesIds)

        firstNavHostFragment = navHostFragments[0]

        firstNavHostFragmentTag = getNavHostFragmentTag(0)

        selectedNavHostFragmentTag = firstNavHostFragmentTag

        fragmentManager.showFirstNavHostFragment()
        fragmentManager.initOuterBackStackChangeListener()

        bottomNavigationView.enableMenuItemSelectionListener()
    }

    private fun initMenuItemIdToNavHostFragmentTagMap(menuItemIdToNavGraphResourceIdMap: Map<Int, Int>) {
        menuItemIdToNavGraphResourceIdMap.keys.forEachIndexed { index, menuItemId ->
            menuItemIdToNavHostFragmentTagMap[menuItemId] = getNavHostFragmentTag(index)
        }
    }

    /**
     * Makes [firstNavHostFragment] visible in the activity it was added to, while hiding all other [NavHostFragment]
     * instances found in [navHostFragments]
     */
    private fun FragmentManager.showFirstNavHostFragment() {
        beginTransaction()
                .apply {
                    navHostFragments.forEach { navHostFragment ->
                        if (navHostFragment == firstNavHostFragment) {
                            attach(navHostFragment)

                            /* Allow our (inner) nav host fragment to have its own backstack (not exactly necessary, if
                            our first nav host fragment has a single, default destination (as is currently the case
                            with the home nav fragment))*/
                            setPrimaryNavigationFragment(navHostFragment)
                        } else {
                            detach(navHostFragment)
                        }
                    }
                }
                .commitNow()
    }

    /**
     * Initializes code that runs when the outer, host activity, backstack changes
     */
    private fun FragmentManager.initOuterBackStackChangeListener() {
        addOnBackStackChangedListener {
            /* If this evaluates to false, the user pressed the back button, after having previously selected a tab.
             That means, the "secondary nav host fragment" transaction was reverted, and the first navigation fragment
             is yet again visible. All we have to do is update the bottom navigation's view selected tab
             */

            if (!fragmentManager.isSecondaryNavHostFragmentInBackStack()) {
                with(bottomNavigationView) {
                    disableMenuItemSelectionListener()
                    selectedItemId = firstMenuItemId
                    enableMenuItemSelectionListener()
                }
            }
        }
    }

    private fun createNavHostFragments(
            fragmentManager: FragmentManager, navHostFragmentContainerId: Int,
            navGraphResourceIds: Iterable<Int>
    ): List<NavHostFragment> {
        return navGraphResourceIds.mapIndexed { index, navGraphResourceId ->
            createNavHostFragment(
                    fragmentManager,
                    getNavHostFragmentTag(index),
                    navGraphResourceId,
                    navHostFragmentContainerId
            )
        }
    }

    private fun getNavHostFragmentTag(navHostFragmentIndex: Int): String {
        return "nav_host_fragment_$navHostFragmentIndex"
    }

    /**
     * Creates a new [NavHostFragment] instance, that will be associated with the given navigation graph
     * ([navGraphResourceId]), and added to the given [fragmentManager] using the specified tag
     * ([navHostFragmentTag]) and view container id ([navHostFragmentContainerId])
     */
    private fun createNavHostFragment(
            fragmentManager: FragmentManager,
            navHostFragmentTag: String,
            navGraphResourceId: Int,
            navHostFragmentContainerId: Int
    ): NavHostFragment {
        /*
        Create a new NavHostFragment, with the given navigation graph, equivalent to setting the navigation graph via
        app:navGraph="@navigation/navigation_graph_product_selection"
        */
        val navHostFragment = NavHostFragment.create(navGraphResourceId)

        fragmentManager.beginTransaction()
                .add(navHostFragmentContainerId, navHostFragment, navHostFragmentTag)
                .commitNow()

        return navHostFragment
    }

    private fun BottomNavigationView.enableMenuItemSelectionListener() {
        setOnNavigationItemSelectedListener(navigationItemSelectedListener)
    }

    private fun BottomNavigationView.disableMenuItemSelectionListener() {
        setOnNavigationItemSelectedListener(null)
    }

    /**
     * Returns [true], if [fragmentManager] contains an entry named after
     * [SECONDARY_NAV_HOST_FRAGMENT_BACK_STACK_ENTRY_NAME]
     */
    private fun FragmentManager.isSecondaryNavHostFragmentInBackStack(): Boolean {
        return (0 until backStackEntryCount).any { index ->
            getBackStackEntryAt(index).name == SECONDARY_NAV_HOST_FRAGMENT_BACK_STACK_ENTRY_NAME
        }
    }

    companion object {
        private const val SECONDARY_NAV_HOST_FRAGMENT_BACK_STACK_ENTRY_NAME = "secondary_nav_host_fragment"
    }

    private inner class NavigationItemSelectedListener : BottomNavigationView.OnNavigationItemSelectedListener {
        override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
            val selectionCandidateNavHostFragmentTag = menuItemIdToNavHostFragmentTagMap.getValue(menuItem.itemId)

            if (selectionCandidateNavHostFragmentTag != selectedNavHostFragmentTag) {
                /* If a new selection is made, revert the old transaction created by the previous selection
                * (if any) */

                fragmentManager.popBackStack(
                        SECONDARY_NAV_HOST_FRAGMENT_BACK_STACK_ENTRY_NAME, FragmentManager.POP_BACK_STACK_INCLUSIVE
                )

                selectedNavHostFragmentTag = selectionCandidateNavHostFragmentTag

                /* If the user taps the first tab, there's no point in creating a new backstack entry for the first
                 nav host fragment, as it will already be visible due to the above pop operation, after which only
                 the first fragment will remain attached to the activity */
                if (selectedNavHostFragmentTag != firstNavHostFragmentTag) {
                    val selectedNavHostFragment = fragmentManager.findFragmentByTag(selectedNavHostFragmentTag)!!

                    fragmentManager.beginTransaction()
                            // Attach this nav host fragment, to the activity, make it visible
                            .attach(selectedNavHostFragment)

                            // Allow our (inner) nav host fragment to have its own backstack
                            .setPrimaryNavigationFragment(selectedNavHostFragment)
                            .apply {
                                // Hide all other nav host fragments
                                menuItemIdToNavHostFragmentTagMap.map { it.value }
                                        .filterNot { it == selectedNavHostFragmentTag }
                                        .forEach { detach(fragmentManager.findFragmentByTag(it)!!) }
                            }
                            .addToBackStack(SECONDARY_NAV_HOST_FRAGMENT_BACK_STACK_ENTRY_NAME)
                            .commit()
                }
            }

            return true // Will display the tapped menu item as selected
        }
    }
}